import config from "config";
import amqp from "amqplib/callback_api";
import debug from "debug";
import { json } from "express";

var channel: amqp.Channel;
const print = debug("node:amqp");
const findSimilarLikedSongsQueue = String(
  config.get("RABBITMQ.likedSongsQueue")
);
const findSimilarPlaylistSongsQueue = String(
  config.get("RABBITMQ.playlistSongsQueue")
);

amqp.connect(
  {
    hostname: config.get("RABBITMQ.host"),
    port: config.get("RABBITMQ.port"),
    protocol: config.get("RABBITMQ.protocol"),
  },
  (connectionError, connection) => {
    if (connectionError) throw connectionError;
    connection.createChannel((channelError, c) => {
      if (channelError) throw channelError;
      print("app is connected to rabbitmq");

      channel = c;
      channel.assertQueue(findSimilarLikedSongsQueue);
      channel.assertQueue(findSimilarPlaylistSongsQueue);

      sendToSimilarLikedSongs({
        msg: "hello world",
      });
    });
  }
);

export function sendToSimilarLikedSongs(json: any) {
  channel.sendToQueue(
    findSimilarLikedSongsQueue,
    Buffer.from(JSON.stringify(json))
  );
}

export function sendToSimilarPlaylistSongs(params: any) {
  channel.sendToQueue(
    findSimilarPlaylistSongsQueue,
    Buffer.from(JSON.stringify(json))
  );
}
