import rateLimit from "express-rate-limit";
import express, { Request, Response } from "express";
import { createUser, login, refresh } from "../../services/user";
import { CustomError, wrongCredentials } from "../../utils/errors";
import { checkLogin } from "../tools/checks";
import Joi from "joi";
const router = express.Router();
const apiLimiter = rateLimit({
  windowMs: 60 * 60 * 1000, // 60 minutes
  max: 1,
});

router.post("/", apiLimiter, async (req: Request, res: Response) => {
  req.body.role = "user";
  const user = await createUser(req.body);
  res.send(user);
});

router.post("/login", async (req: Request, res: Response) => {
  const schema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
  });
  const validate = schema.validate(req.body);
  if (validate.error) throw new CustomError(wrongCredentials);

  const token = await login(req.body.username, req.body.password);
  if (!token) throw new CustomError(wrongCredentials);
  res.json({
    token,
  });
});

router.get("/refresh", checkLogin, async (req: any, res: Response) => {
  const token = await refresh(req.tokenBody.id);
  if (!token) throw new CustomError(wrongCredentials);
  res.json({
    token,
  });
});

//todo get user info

export = router;
