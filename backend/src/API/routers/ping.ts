import express from "express";

const router = express.Router();

router.get("/", (req, res) => {
  res.json({
    ping: "pong",
  });
});

router.post("/", (req, res) => {
  res.json({
    ping: req.body,
  });
});

export = router;
