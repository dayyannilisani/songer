import express, { Request, Response } from "express";
import Joi from "joi";
import * as service from "../../services/album";
import { CustomError, invalidInput } from "../../utils/errors";
import { checkAdmin, requiredFile } from "../tools/checks";
import upload from "../tools/uploads";

const router = express.Router();

router.post(
  "/",
  checkAdmin,
  upload.single("pic"),
  requiredFile,
  async (req: Request, res: Response) => {
    req.body.pic = req.file.filename;
    req.body.tags = req.body.tags ? req.body.tags.split(",") : [];
    req.body.artists = req.body.artists ? req.body.artists.split(",") : [];
    const schema = Joi.object({
      pic: Joi.string().required(),
      name: Joi.string().required(),
      tags: Joi.array().items(Joi.number()).required(),
      artists: Joi.array().items(Joi.number()).required(),
    });
    const validate = schema.validate(req.body);
    if (validate.error) throw new CustomError(invalidInput);
    const artist = await service.create(req.body);
    res.json(artist);
  }
);

router.get("/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  const artist = await service.get(id);
  res.json(artist);
});

router.get("/:offset/:limit", async (req, res) => {
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const artists = await service.getList(offset, limit);
  res.json(artists);
});

router.get("/search/:search/:offset/:limit", async (req, res) => {
  const search = req.params.search;
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const artist = await service.search(search, offset, limit);
  res.json(artist);
});

export = router;
