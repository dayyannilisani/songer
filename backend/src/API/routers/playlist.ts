import express from "express";
import Joi from "joi";
import { sendToSimilarPlaylistSongs } from "../../pipes/rabbitmq";
import * as service from "../../services/playlist";
import { CustomError, invalidInput, unauthorized } from "../../utils/errors";
import { checkLogin } from "../tools/checks";

const router = express.Router();

router.post("/", checkLogin, async (req, res) => {
  const schema = Joi.object({
    name: Joi.string(),
    user: Joi.number(),
    songs: Joi.array().items(Joi.number()),
  });
  if (schema.validate(req.body).error) throw new CustomError(invalidInput);
  const playlist = await service.create(req.body);
  sendToSimilarPlaylistSongs({
    playlistId: (playlist as any).id,
  });
  res.json(playlist);
});

router.post("/song/:id", checkLogin, async (req, res) => {
  const id = parseInt(req.params.id);
  const schema = Joi.object({
    songs: Joi.array().items(Joi.number()),
  });
  if (schema.validate(req.body).error) throw new CustomError(invalidInput);
  const result = await service.addSong(
    (req as any).tokenBody.id,
    id,
    req.body.songs
  );
  if (!result) throw new CustomError(unauthorized);
  sendToSimilarPlaylistSongs({
    playlistId: id,
  });
  res.json(result);
});

router.get("/:id", async (req, res) => {
  const id = parseInt(req.params.id);
  const playlist = await service.get(id);
  res.json(playlist);
});

router.get("/:offset/:limit", async (req, res) => {
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const playlists = await service.getList(offset, limit);
  res.json(playlists);
});

router.get("/search/:search/:offset/:limit", async (req, res) => {
  const search = req.params.search;
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const playlists = await service.search(search, offset, limit);
  res.json(playlists);
});

router.get("/mine/:offset/:limit", checkLogin, async (req, res) => {
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const playlists = await service.getMyList(
    (req as any).tokenBody.id,
    offset,
    limit
  );
  res.json(playlists);
});

router.get("/similars/:playlistId/:offset/:limit", async (req, res) => {
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const playlistId = parseInt(req.params.playlistId);
  const result = await service.getSimilarPlaylistSongs(
    playlistId,
    offset,
    limit
  );
  res.json(result);
});

router.delete("/:id", checkLogin, async (req, res) => {
  const id = parseInt(req.params.id);
  const result = await service.destroy((req as any).tokenBody.id, id);
  res.json();
});

export = router;
