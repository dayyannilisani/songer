import express from "express";
import config from "config";
import * as service from "../../services/song";
import print from "../../utils/print";
import upload from "../tools/uploads";
import get_audio_duration from "get-audio-duration";
import { CustomError, invalidInput } from "../../utils/errors";
import Joi, { required } from "joi";
import { checkAdmin, checkLogin } from "../tools/checks";
import { sendToSimilarLikedSongs } from "../../pipes/rabbitmq";

const musicsLocation = config.get("API.upload.musics.storage");

const router = express.Router();

router.post(
  "/",
  checkAdmin,
  upload.fields([
    { name: "pic", maxCount: 1 },
    { name: "song", maxCount: 1 },
  ]),
  async (req: any, res) => {
    req.body.tags = req.body.tags ? req.body.tags.split(",") : [];
    req.body.artists = req.body.artists ? req.body.artists.split(",") : [];
    req.body.pic = req.files.pic ? req.files.pic[0].filename : null;
    req.body.song = req.files.song ? req.files.song[0].filename : null;
    req.body.duration = await get_audio_duration(
      musicsLocation + req.files.song[0].filename
    );
    const schema = Joi.object({
      name: Joi.string(),
      tags: Joi.array().items(Joi.number()),
      artists: Joi.array().items(Joi.number()),
      AlbumId: Joi.number().allow(null),
      duration: Joi.number(),
      pic: Joi.string().allow(null),
      song: Joi.string().required(),
    });
    if (schema.validate(req.body).error) throw new CustomError(invalidInput);
    const result = await service.create(req.body);
    res.json(result);
  }
);

router.get("/:id", async (req: any, res) => {
  const id = parseInt(req.params.id);
  const userId = req.tokenBody ? req.tokenBody.id : 0;
  const song = await service.get(id, userId);
  res.json(song);
});

router.get("/:offset/:limit", async (req, res) => {
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const results = await service.getList(offset, limit);
  res.json(results);
});

router.get("/search/:search/:offset/:limit", async (req, res) => {
  const search = req.params.search;
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const results = await service.search(search, offset, limit);
  res.json(results);
});

router.post("/like/:id", checkLogin, async (req, res) => {
  const id = parseInt(req.params.id);
  const result = await service.like((req as any).tokenBody.id, id);
  sendToSimilarLikedSongs({
    userId: (req as any).tokenBody.id,
  });
  res.json(result);
});

router.get("/like/:offset/:limit", checkLogin, async (req, res) => {
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const result = await service.getLikedSongs(
    (req as any).tokenBody.id,
    offset,
    limit
  );
  res.json(result);
});

router.get("/like/similars/:offset/:limit", checkLogin, async (req, res) => {
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const result = await service.getSimilarLikedSongs(
    (req as any).tokenBody.id,
    offset,
    limit
  );
  res.json(result);
});

export = router;
