import express from "express";
import Joi from "joi";
import * as service from "../../services/tag";
import { CustomError, invalidInput } from "../../utils/errors";
import { checkAdmin } from "../tools/checks";

const router = express.Router();

router.post("/", checkAdmin, async (req, res) => {
  const schema = Joi.object({
    names: Joi.array()
      .items(Joi.object({ name: Joi.string().required() }))
      .required(),
  });
  const validate = schema.validate(req.body);
  if (validate.error) throw new CustomError(invalidInput);

  const results = await service.create(req.body.names);
  res.json(results);
});

router.get("/:offset/:limit", async (req, res) => {
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const results = await service.getList(offset, limit);
  res.json(results);
});

router.get("/search/:search/:offset/:limit", async (req, res) => {
  const search = req.params.search;
  const offset = parseInt(req.params.offset);
  const limit = parseInt(req.params.limit);
  const results = await service.search(search, offset, limit);
  res.json(results);
});

export = router;
