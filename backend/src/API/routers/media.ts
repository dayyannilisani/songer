import express from "express";
import config from "config";
import fs from "fs";
import { CustomError, invalidInput } from "../../utils/errors";
import print from "../../utils/print";
const imageStorage = config.get("API.upload.images.storage");
const musicStorage = config.get("API.upload.musics.storage");
const router = express.Router();

router.get("/image/:name", (req, res) => {
  if (
    req.params.name.toString().includes("..") ||
    req.params.name.toString().toString().includes("/") ||
    !fs.existsSync(imageStorage + req.params.name)
  )
    throw new CustomError(invalidInput);

  res.sendFile(imageStorage + req.params.name, {
    root: ".",
  });
});

router.get("/music/:name", (req, res) => {
  // range example  -- > bytes=2000004-
  const range = req.headers.range;
  if (
    !range ||
    req.params.name.toString().includes("..") ||
    req.params.name.toString().toString().includes("/") ||
    !fs.existsSync(musicStorage + req.params.name)
  )
    throw new CustomError(invalidInput);

  const audioPath = musicStorage + req.params.name;
  const audioSize = fs.statSync(audioPath).size;

  const CHUNK_SIZE = 10 ** 6 / 2; // 500KB
  const start = Number(range.toString().replace(/\D/g, ""));
  const end = Math.min(start + CHUNK_SIZE, audioSize - 1);

  const contentLength = end - start + 1;
  const headers = {
    "Content-Range": `bytes ${start}-${end}/${audioSize}`,
    "Accept-Ranges": "bytes",
    "Content-Length": contentLength,
    "Content-Type": "audio/mp3",
  };

  res.writeHead(206, headers);

  const audioStream = fs.createReadStream(audioPath, {
    start,
    end,
  });

  audioStream.pipe(res);
});

export = router;
