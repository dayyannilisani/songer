import * as jwt from "jsonwebtoken";
import config from "config";
import { CustomError, invalidToken } from "../../utils/errors";
import { Response, NextFunction, Request } from "express";

async function auth(req: any, res: Response, next: NextFunction) {
  let token = "";
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }

  if (!token) {
    return next();
  }

  try {
    const decoded: any = jwt.verify(token, config.get("API.jwtPassword"));
    req.tokenBody = {
      id: decoded.id,
      role: decoded.role,
    };
    next();
  } catch (err) {
    console.log(err);
    throw new CustomError(invalidToken);
  }
}
export = auth;
