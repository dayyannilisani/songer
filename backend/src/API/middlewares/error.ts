import debug from "debug";
import {
  CustomError,
  internalServerError,
  invalidInput,
} from "../../utils/errors";
import { Response, Request, NextFunction } from "express";
import { BaseError as SequelizeBaseError } from "sequelize";
import logError from "../../utils/log";

const print = debug("node:error");
export = async function error(
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) {
  print(err);
  if (err instanceof CustomError) {
    const status = (err as CustomError).status;
    return res.status(status).json(err);
  } else {
    if (err instanceof SequelizeBaseError) {
      return res.status(invalidInput.status).json(invalidInput);
    } else {
      logError(err);
      return res.status(internalServerError.status).json({
        code: internalServerError.code,
        message: internalServerError.message,
      });
    }
  }
};
