import debug from 'debug'

const print = debug("node:reqs")

function logMiddleware(req:any, res:any, next:any) {

    var oldWrite = res.write,
        oldEnd = res.end;

    var chunks:any[] = [];

    res.write = function (chunk:string) {
        chunks.push(chunk);

        oldWrite.apply(res, arguments);
    };

    res.end = function (chunk:string) {
        if (chunk) chunks.push(chunk);
        try {
            var body = Buffer.concat(chunks).toString("utf8");
            var request = ""
            if (JSON.stringify(req.body))
                request = JSON.stringify(req.body)
            var logMessage = `\nurl = ${req.url}\nmethod = ${req.method}\nrequest body = {${request}}\nresponse body = {${body}}\n---------------------------`
            print(logMessage)
        } catch (err) {}

        oldEnd.apply(res, arguments);
    };

    next();
};

export = logMiddleware