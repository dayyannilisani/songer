import express from "express";
import helmet from "helmet";
import cors from "cors";
import config from "config";
import debug from "debug";
import compression from "compression";
import "express-async-errors";

import authM from "./middlewares/auth";
import errorM from "./middlewares/error";
import logM from "./middlewares/log";
import ping from "./routers/ping";
import user from "./routers/user";
import artist from "./routers/artist";
import tag from "./routers/tag";
import album from "./routers/album";
import song from "./routers/song";
import media from "./routers/media";
import playlist from "./routers/playlist";

const print = debug("node:api");
const port = config.get("API.port");
const logRequest = config.get("API.logRequests");
const app = express();

app.use(compression());
app.use(express.json());
app.use(helmet());
app.use(cors());

if (logRequest) {
  app.use(logM);
}
app.use(authM);

app.use("/ping", ping);
app.use("/user", user);
app.use("/artist", artist);
app.use("/tag", tag);
app.use("/album", album);
app.use("/song", song);
app.use("/media", media);
app.use("/playlist", playlist);

app.use(errorM);

const server = app.listen(port, () => {
  print("server is listening on " + port);
});

export = server;
