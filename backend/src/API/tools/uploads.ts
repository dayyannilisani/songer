import multer from "multer";
import config from "config";
import path from "path";
import { v4 } from "uuid";
import { CustomError, invalidInput } from "../../utils/errors";
import print from "../../utils/print";

const musicsConfig: any = config.get("API.upload.musics");
const imagesConfig: any = config.get("API.upload.images");
const imagesExt = imagesConfig["allowedExtensions"];
const imagesMaxSize = imagesConfig["maxFileSize"];
const musicsExt = musicsConfig["allowedExtensions"];
const musicsMaxSize = musicsConfig["maxFileSize"];

const uploadDiskStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    const ext = path.extname(file.originalname);
    if (imagesExt.includes(ext)) cb(null, imagesConfig["storage"]);
    else if (musicsExt.includes(ext)) cb(null, musicsConfig["storage"]);
  },
  filename: (req, file, cb) => {
    cb(null, v4() + file.originalname);
  },
});

const upload = multer({
  storage: uploadDiskStorage,
  fileFilter: function (req, file, cb) {
    const ext = path.extname(file.originalname);

    if (
      (!imagesExt.includes(ext) || file.size > imagesMaxSize) &&
      (!musicsExt.includes(ext) || file.size > musicsMaxSize)
    ) {
      return cb(new CustomError(invalidInput));
    }
    cb(null, true);
  },
});

export = upload;
