import { NextFunction, Request, Response } from "express";
import {
  CustomError,
  invalidInput,
  loginRequired,
  unauthorized,
} from "../../utils/errors";

export function checkLogin(req: any, res: Response, next: NextFunction) {
  if (!req.tokenBody) throw new CustomError(loginRequired);
  next();
}
export function checkAdmin(req: any, res: Response, next: NextFunction) {
  if (!req.tokenBody || req.tokenBody.role != "admin")
    throw new CustomError(unauthorized);
  next();
}

export function requiredFile(req: Request, res: Response, next: NextFunction) {
  if (!req.file && !req.files) throw new CustomError(invalidInput);
  next();
}
