import winston from 'winston'

const logger = winston.createLogger({
    format: winston.format.simple(),
    transports: [
        new winston.transports.File({
            filename: './log/errors.log',
        }),
    ],
})

function logError(err:Error) {
        logger.log({
            level: 'error',
            message: `${err.message}\n${err.stack}`
        })
        logger.log({
            level: 'warn',
            message: '-----------------------------------------------'
        })
}

export = logError