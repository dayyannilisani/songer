interface MyError {
  message: string;
  code: number;
  status: number;
}
export class CustomError extends Error {
  message: string;
  code: number;
  status: number;
  constructor(error: MyError, message?: string) {
    super();
    this.code = error.code;
    this.message = error.message;
    this.status = error.status;
    if (message) {
      this.message = message;
    }
  }
}

export const internalServerError: MyError = {
  message: "Internal server Error",
  code: 0,
  status: 500,
};

export const invalidToken: MyError = {
  message: "invalid token",
  code: 1,
  status: 401,
};

export const loginRequired: MyError = {
  message: "login required",
  code: 2,
  status: 401,
};

export const unauthorized: MyError = {
  message: "unauthorized",
  code: 3,
  status: 403,
};

export const wrongCredentials: MyError = {
  message: "wrong credentials",
  code: 4,
  status: 401,
};

export const invalidInput: MyError = {
  message: "invalid inputs",
  code: 5,
  status: 400,
};
