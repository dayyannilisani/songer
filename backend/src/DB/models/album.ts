import { Model, DataTypes } from "sequelize";
import sequelize from "../connection";

class Album extends Model {}

Album.init(
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    pic: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    createdAt: false,
    updatedAt: false,
  }
);

export = Album;
