import { Model, DataTypes } from "sequelize";
import sequelize from "../connection";

class Song extends Model {}

Song.init(
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    duration: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    song: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    pic: {
      type: DataTypes.STRING,
    },
    listenedToCount: {
      defaultValue: 0,
      type: DataTypes.INTEGER,
    },
  },
  {
    sequelize,
    createdAt: false,
    updatedAt: false,
  }
);

export = Song;
