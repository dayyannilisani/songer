import { Model, DataTypes } from "sequelize";
import sequelize from "../connection";

class Tag extends Model {}

Tag.init(
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  },
  {
    sequelize,
    createdAt: false,
    updatedAt: false,
  }
);
export = Tag;
