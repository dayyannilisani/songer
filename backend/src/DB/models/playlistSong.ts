import { DataTypes, Model } from "sequelize";
import sequelize from "../connection";
import Playlist from "./playlist";
import Song from "./song";

class PlaylistSongs extends Model {}

PlaylistSongs.init(
  {
    song: {
      type: DataTypes.INTEGER,
      references: {
        model: Song,
        key: "id",
      },
      allowNull: false,
      onDelete: "cascade",
    },
    playlist: {
      type: DataTypes.INTEGER,
      references: {
        model: Playlist,
        key: "id",
      },
      onDelete: "cascade",
      allowNull: false,
    },
  },
  {
    sequelize,
    indexes: [
      {
        unique: true,
        fields: ["song", "playlist"],
      },
    ],
  }
);

export = PlaylistSongs;
