import { DataTypes, Model } from "sequelize";
import sequelize from "../connection";
import Song from "./song";
import User from "./user";

class SimilarLikedSongs extends Model {}

SimilarLikedSongs.init(
  {
    song: {
      type: DataTypes.INTEGER,
      references: {
        model: Song,
        key: "id",
      },
      allowNull: false,
      onDelete: "cascade",
    },
    user: {
      type: DataTypes.INTEGER,
      references: {
        model: User,
        key: "id",
      },
      onDelete: "cascade",
      allowNull: false,
    },
  },
  {
    sequelize,
    createdAt: false,
    updatedAt: false,
    indexes: [
      {
        unique: true,
        fields: ["song", "user"],
      },
    ],
  }
);

export = SimilarLikedSongs;
