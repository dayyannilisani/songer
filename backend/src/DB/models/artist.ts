import { Model, DataTypes } from "sequelize";
import sequelize from "../connection";

class Artist extends Model {}

Artist.init(
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    pic: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    createdAt: false,
    updatedAt: false,
  }
);

export = Artist;
