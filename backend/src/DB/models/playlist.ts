import { Model, DataTypes } from "sequelize";
import sequelize from "../connection";

class Playlist extends Model {}

Playlist.init(
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isPrivate: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
  },
  {
    sequelize,
    createdAt: false,
    updatedAt: false,
  }
);

export = Playlist;
