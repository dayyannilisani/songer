import fs from "fs/promises";
import debug from "debug";

const print = debug("node:db");

import sequelize from "./connection";
import User from "./models/user";
import Artist from "./models/artist";
import Album from "./models/album";
import Tag from "./models/tag";
import Song from "./models/song";
import Playlist from "./models/playlist";
import dataInit from "../tests/dataInit";
import PlaylistSongs from "./models/playlistSong";
import SimilarPlaylistSongs from "./models/playlistSimilarSong";
import LikedSongs from "./models/LikedSong";
import SimilarLikedSongs from "./models/SimilarLikedSong";

// Relations
// 1 -> album - artist
// 2 -> album - song
// 3 -> album - tag
// 4 -> song - tag
// 5 -> artist - song
// 6 -> artist - tag
// 7 -> playlist - song
// 8 -> user - playlist
// 9 -> user - song
// 10 -> user - artist
// 11 -> playlist - song similarity
// 12 -> user - song similarity

// 1 --------------
Album.belongsToMany(Tag, {
  through: "Album_Tag",
  onDelete: "CASCADE",
});
Artist.belongsToMany(Album, {
  through: "Album_Artist",
  onDelete: "CASCADE",
});

// 2 --------------
Album.hasMany(Song);
Song.belongsTo(Album);

// 3 --------------
Album.belongsToMany(Artist, {
  through: "Album_Artist",
  onDelete: "CASCADE",
});
Tag.belongsToMany(Album, {
  through: "Album_Tag",
  onDelete: "CASCADE",
});

// 4 --------------
Song.belongsToMany(Tag, {
  through: "Song_Tag",
  onDelete: "CASCADE",
});
Tag.belongsToMany(Song, {
  through: "Song_Tag",
  onDelete: "CASCADE",
});

// 5 -------------
Artist.belongsToMany(Song, {
  through: "Song_Artist",
  onDelete: "CASCADE",
});
Song.belongsToMany(Artist, {
  through: "Song_Artist",
  onDelete: "CASCADE",
});

// 6 -----------
Artist.belongsToMany(Tag, {
  through: "Artist_Tag",
  onDelete: "CASCADE",
});
Tag.belongsToMany(Artist, {
  through: "Artist_Tag",
  onDelete: "CASCADE",
});

// 7 ------------
Playlist.belongsToMany(Song, {
  through: PlaylistSongs,
  foreignKey: "playlist",
  as: "songs",
});
Song.belongsToMany(Playlist, {
  through: PlaylistSongs,
  foreignKey: "song",
  as: "songs",
});

// 8 ------------
User.hasMany(Playlist);
Playlist.belongsTo(User);

// 9 ------------
User.belongsToMany(Song, {
  through: LikedSongs,
  onDelete: "CASCADE",
  foreignKey: "user",
  as: "LS",
});
Song.belongsToMany(User, {
  through: LikedSongs,
  onDelete: "CASCADE",
  foreignKey: "song",
  as: "LS",
});

// 10 ------------
User.belongsToMany(Artist, {
  through: "User_Follow_Artist",
  onDelete: "CASCADE",
});
Artist.belongsToMany(User, {
  through: "User_Follow_Artist",
  onDelete: "CASCADE",
});

// 11 ------------
Playlist.belongsToMany(Song, {
  through: SimilarPlaylistSongs,
  foreignKey: "playlist",
  as: "SPS",
});
Song.belongsToMany(Playlist, {
  through: SimilarPlaylistSongs,
  foreignKey: "song",
  as: "SPS",
});

// 12 ------------
Song.belongsToMany(User, {
  through: SimilarLikedSongs,
  foreignKey: "song",
  as: "SLS",
});
User.belongsToMany(Song, {
  through: SimilarLikedSongs,
  foreignKey: "user",
  as: "SLS",
});

async function initDB() {
  const force = true;
  await sequelize.authenticate();
  await sequelize.sync({
    force,
  });
  if (force) {
    try {
      await fs.rm("./upload/images", { recursive: true, force: true });
      await fs.mkdir("./upload/images");
      await fs.rm("./upload/musics", { recursive: true, force: true });
      await fs.mkdir("upload/musics");
      await dataInit();
    } catch (e) {
      print(e);
    }
  }
  print("connection to DB is successful");
}

initDB();
