import { Sequelize } from "sequelize";
import config from "config";
import print from "../utils/print";

const dbInfo = JSON.parse(JSON.stringify(config.get("DB")));

const sequelize = new Sequelize(
  dbInfo["database"],
  dbInfo["username"],
  dbInfo["password"],
  {
    dialect: dbInfo["dialect"],
    host: dbInfo["host"],
    port: dbInfo["port"],
    logging: false,
  }
);

export = sequelize;
