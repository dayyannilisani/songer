import { Op, Sequelize } from "sequelize";
import config from "config";
import fs from "fs";
import Artist from "../DB/models/artist";
import sequelize from "../DB/connection";
import Tag from "../DB/models/tag";
import Song from "../DB/models/song";

const imageStorage = config.get("API.upload.images.storage");

export async function create(body: any) {
  const transaction = await sequelize.transaction();
  try {
    const artist = await Artist.create(body, { transaction });
    await (artist as any).setTags(body.tags, { transaction });
    await transaction.commit();
    return artist;
  } catch (err) {
    await transaction.rollback();
    fs.unlinkSync(imageStorage + body.pic);
    throw err;
  }
}

export async function get(id: number) {
  const artist = await Artist.findByPk(id, {
    attributes: [
      "id",
      "name",
      "pic",
      [Sequelize.fn("COUNT", Sequelize.col("Songs.id")), "songsCount"],
      [
        Sequelize.fn("SUM", Sequelize.col("Songs.listenedToCount")),
        "totalPlayed",
      ],
    ],
    include: [
      {
        model: Song,
        attributes: [],
        through: {
          attributes: [],
        },
      },
      {
        model: Tag,
        attributes: ["id", "name"],
        through: {
          attributes: [],
        },
      },
    ],
    group: ["Artist.id", "Tags.id"],
  });
  // todo : should fetch additional info
  return artist;
}

export async function getList(offset: number, limit: number) {
  const artists = await Artist.findAll({
    offset,
    limit,
  });
  return artists;
}

export async function search(search: string, offset: number, limit: number) {
  const artists = await Artist.findAll({
    offset,
    limit,
    where: {
      name: {
        [Op.like]: `%${search}%`,
      },
    },
  });
  return artists;
}
