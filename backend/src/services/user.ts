import User from "../DB/models/user";
import bcrypt from "bcrypt";
import jwt, { Secret } from "jsonwebtoken";
import config from "config";

const jwtPassword: string = config.get("API.jwtPassword");
const jwtExpiresIn: string = config.get("API.jwtExpiresIn");

export async function createUser(body: any) {
  if (body.password) {
    const salt = await bcrypt.genSalt(10);
    body.password = await bcrypt.hash(body.password, salt);
  }
  let user = await User.create(body);
  user = JSON.parse(JSON.stringify(user));
  delete (user as any).password;
  return user;
}

export async function login(username: string, password: string) {
  const user = await User.findOne({ where: { username } });
  if (user) {
    const passwordCheck = bcrypt.compare(password, (user as any).password);
    if (!passwordCheck) return false;
    const token = await jwt.sign(
      {
        id: (user as any).id,
        role: (user as any).role,
      },
      jwtPassword,
      {
        expiresIn: jwtExpiresIn,
      }
    );
    return token;
  } else {
    return false;
  }
}

export async function refresh(id: number) {
  const user = await User.findByPk(id);
  if (user) {
    const token = await jwt.sign(
      {
        id: (user as any).id,
        role: (user as any).role,
      },
      jwtPassword,
      {
        expiresIn: jwtExpiresIn,
      }
    );
    return token;
  }
}

// todo get user info
