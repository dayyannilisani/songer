import Tag from "../DB/models/tag";
import { Op } from "sequelize";

export async function create(body: any) {
  const results = await Tag.bulkCreate(body);
  return results;
}

export async function getList(offset: number, limit: number) {
  const results = await Tag.findAll({
    offset,
    limit,
  });
  return results;
}

export async function search(search: string, offset: number, limit: number) {
  const results = await Tag.findAll({
    offset,
    limit,
    where: {
      name: {
        [Op.like]: `%${search}%`,
      },
    },
  });
  return results;
}
