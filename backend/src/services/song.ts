import sequelize from "../DB/connection";
import { Sequelize, Op } from "sequelize";
import config from "config";
import fs from "fs";
import Album from "../DB/models/album";
import Song from "../DB/models/song";
import Tag from "../DB/models/tag";
import Artist from "../DB/models/artist";
import User from "../DB/models/user";
import print from "../utils/print";
import SimilarLikedSongs from "../DB/models/SimilarLikedSong";
import LikedSongs from "../DB/models/LikedSong";

const imageStorage = config.get("API.upload.images.storage");
const musicStorage = config.get("API.upload.musics.storage");

export async function create(body: any) {
  const transaction = await sequelize.transaction();
  const picInitialValue = body.pic;
  try {
    const album = await Album.findByPk(body.AlbumId, {
      attributes: [],
      include: [
        {
          model: Tag,
          attributes: ["id"],
          through: {
            attributes: [],
          },
        },
        {
          model: Artist,
          attributes: ["id"],
          through: {
            attributes: [],
          },
        },
      ],
    });
    if (!body.pic && album) {
      if (album) {
        body.pic = (album as any).pic;
      }
    }
    if (!body.tags.length && album) {
      for (var tag in (album as any).Tags) {
        body.tags.push((album as any).Tags[tag].id);
      }
    }
    if (!body.artists.length && album) {
      for (var artist in (album as any).Artists) {
        body.artists.push((album as any).Artists[artist].id);
      }
    }
    const song = await Song.create(body, { transaction });
    await (song as any).setTags(body.tags, { transaction });
    await (song as any).setArtists(body.artists, { transaction });
    await transaction.commit();
    return song;
  } catch (err) {
    await transaction.rollback();
    if (picInitialValue) fs.unlinkSync(imageStorage + picInitialValue);
    fs.unlinkSync(musicStorage + body.song);
    throw err;
  }
}

export async function get(id: number, userId: number) {
  const song = await Song.findByPk(id, {
    attributes: [
      "id",
      "name",
      "duration",
      "song",
      "pic",
      "listenedToCount",
      [Sequelize.fn("COUNT", Sequelize.col("Users.id")), "likes"],
      [
        Sequelize.literal(
          `exists(select * from "Liked_Song" where "UserId" = ${userId})`
        ),
        "likedByYou",
      ],
    ],
    include: [
      {
        model: Artist,
        through: {
          attributes: [],
        },
      },
      {
        model: User,
        attributes: [],
        through: {
          attributes: [],
        },
      },
      {
        model: Tag,
        through: {
          attributes: [],
        },
      },
      Album,
    ],
    group: ["Song.id", "Artists.id", "Tags.id", "Album.id"],
  });
  if (song) {
    (song as any).listenedToCount++;
    await song.save();
  }
  return song;
}

export async function getList(offset: number, limit: number) {
  const songs = await Song.findAll({
    offset,
    limit,
  });
  return songs;
}

export async function search(search: string, offset: number, limit: number) {
  const songs = await Song.findAll({
    offset,
    limit,
    where: {
      name: {
        [Op.like]: `%${search}%`,
      },
    },
  });
  return songs;
}

export async function like(userId: number, songId: number) {
  const song = await Song.findByPk(songId, {
    include: [
      {
        model: User,
        as: "LS",
        through: {
          attributes: [],
        },
      },
    ],
  });
  if (!song) return false;
  for (var i = 0; i < (song as any).LS.length; i++) {
    if ((song as any).Users[i].id == userId) {
      await (song as any).removeUser(userId);
      return false;
    }
  }
  await LikedSongs.create({
    user: userId,
    song: songId,
  });
  return true;
}

export async function getLikedSongs(
  userId: number,
  offset: number,
  limit: number
) {
  const songs = await Song.findAll({
    offset,
    limit,
    include: [
      {
        model: User,
        attributes: [],
        as: "LS",
        through: {
          attributes: [],
        },
        where: {
          id: userId,
        },
      },
    ],
  });
  return songs;
}

export async function getSimilarLikedSongs(
  userId: number,
  offset: number,
  limit: number
) {
  const songs = await SimilarLikedSongs.findAll({
    where: {
      user: userId,
    },
    offset,
    limit,
    include: [Song],
  });
  return songs;
}
