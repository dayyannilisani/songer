import { Sequelize, Op } from "sequelize";
import sequelize from "../DB/connection";
import Playlist from "../DB/models/playlist";
import SimilarPlaylistSong from "../DB/models/playlistSimilarSong";
import Song from "../DB/models/song";
import User from "../DB/models/user";

export async function create(body: any) {
  const transaction = await sequelize.transaction();
  try {
    const playlist = await Playlist.create(body, { transaction });
    await (playlist as any).setSongs(body.songs);
    await transaction.commit();
    return playlist;
  } catch (err) {
    await transaction.rollback();
    throw err;
  }
}

export async function addSong(
  userId: number,
  playlistId: number,
  songs: number[]
) {
  const playlist = await Playlist.findOne({
    where: {
      id: playlistId,
      user: userId,
    },
  });
  if (!playlist) return null;
  await (playlist as any).addSongs(songs);
  return playlist;
}

export async function destroy(userId: number, playlistId: number) {
  const result = await Playlist.destroy({
    where: {
      id: playlistId,
      user: userId,
    },
  });
  return result;
}

export async function get(id: number) {
  const result = await Playlist.findByPk(id, {
    include: [
      {
        model: User,
        attributes: {
          exclude: ["password"],
        },
      },
      {
        model: Song,
        as: "songs",
        through: {
          attributes: [],
        },
      },
    ],
  });
  return result;
}

export async function getMyList(userId: number, offset: number, limit: number) {
  const result = await Playlist.findAll({
    offset,
    limit,
    where: {
      user: userId,
    },
    attributes: [
      "id",
      "name",
      "user",
      [Sequelize.fn("COUNT", Sequelize.col("Songs.id")), "songsCount"],
    ],
    include: [
      {
        model: Song,
        attributes: [],
        through: {
          attributes: [],
        },
      },
    ],
  });
}

export async function getList(offset: number, limit: number) {
  const result = await Playlist.findAll({
    offset,
    limit,
    where: {
      private: false,
    },
    attributes: [
      "id",
      "name",
      "user",
      [Sequelize.fn("COUNT", Sequelize.col("Songs.id")), "songsCount"],
    ],
    include: [
      {
        model: Song,
        attributes: [],
        through: {
          attributes: [],
        },
      },
    ],
  });
}

export async function search(search: string, offset: number, limit: number) {
  const songs = await Playlist.findAll({
    offset,
    limit,
    where: {
      name: {
        [Op.like]: `%${search}%`,
      },
      private: false,
    },
    attributes: [
      "id",
      "name",
      "user",
      [Sequelize.fn("COUNT", Sequelize.col("Songs.id")), "songsCount"],
    ],
    include: [
      {
        model: Song,
        attributes: [],
        through: {
          attributes: [],
        },
      },
    ],
  });
  return songs;
}

export async function getSimilarPlaylistSongs(
  playlistId: number,
  offset: number,
  limit: number
) {
  const result = await SimilarPlaylistSong.findAll({
    where: {
      playlist: playlistId,
    },
    offset,
    limit,
    include: [Song],
  });
  return result;
}
