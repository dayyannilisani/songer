import fs from "fs";
import config from "config";
import { Op } from "sequelize";
import sequelize from "../DB/connection";
import Album from "../DB/models/album";
import Tag from "../DB/models/tag";
import Artist from "../DB/models/artist";
import Song from "../DB/models/song";

const imageStorage = config.get("API.upload.images.storage");

export async function create(body: any) {
  const transaction = await sequelize.transaction();
  try {
    const album = await Album.create(body, { transaction });
    await (album as any).setTags(body.tags, { transaction });
    await (album as any).setArtists(body.artists, { transaction });
    await transaction.commit();
    return album;
  } catch (err) {
    await transaction.rollback();
    fs.unlinkSync(imageStorage + body.pic);
    throw err;
  }
}

export async function get(id: number) {
  const result = await Album.findByPk(id, {
    include: [
      Song,
      {
        model: Artist,
        through: {
          attributes: [],
        },
      },
      {
        model: Tag,
        through: {
          attributes: [],
        },
      },
    ],
  });
  return result;
}

export async function getList(offset: number, limit: number) {
  const result = await Album.findAll({
    offset,
    limit,
  });
  return result;
}

export async function search(name: string, offset: number, limit: number) {
  const result = await Album.findAll({
    offset,
    limit,
    where: {
      name: {
        [Op.like]: `%${search}%`,
      },
    },
  });
}
